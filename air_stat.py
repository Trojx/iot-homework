#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Trojx(饶建勋) on 2019/1/17
from linkkit import linkkit
import logging
import time
import random
import json
from datetime import datetime

HOST_NAME = 'cn-shanghai'
PRODUCT_KEY = 'a1eSzkLBkT4'
DEVICE_NAME = 'N2lubwwqmGVnmRlbyVzK'
DEVICE_SECRET = 'Tu5wbvFlv3ExzJRUeFoy8HEnhaqwhjKY'
TOPIC = 'AirStat'

# 定义日志格式
__log_format = '%(asctime)s-%(process)d-%(thread)d - %(name)s:%(module)s:%(funcName)s - %(levelname)s - %(message)s'
logging.basicConfig(format=__log_format)

lk = linkkit.LinkKit(
    host_name=HOST_NAME,
    product_key=PRODUCT_KEY,
    device_name=DEVICE_NAME,
    device_secret=DEVICE_SECRET)

lk.enable_logger(logging.DEBUG)


def on_device_dynamic_register(rc, value, userdata):
    """
    动态注册回调
    """
    if rc == 0:
        print("dynamic register device success, rc:%d, value:%s" % (rc, value))
    else:
        print("dynamic register device fail,rc:%d, value:%s" % (rc, value))


def on_connect(session_flag, rc, userdata):
    """
    连接成功回调
    """
    print("on_connect:%d,rc:%d,userdata:" % (session_flag, rc))
    pass


def on_disconnect(rc, userdata):
    """
    连接断开回调
    """
    print("on_disconnect:rc:%d,userdata:" % rc)


def on_topic_message(topic, payload, qos, userdata):
    """
    收到topic订阅消息回调
    """
    print("on_topic_message:" + topic + " payload:" + str(payload) + " qos:" + str(qos))
    pass


def on_subscribe_topic(mid, granted_qos, userdata):
    """
    订阅topic成功回调
    """
    print("on_subscribe_topic mid:%d, granted_qos:%s" %
          (mid, str(','.join('%s' % it for it in granted_qos))))
    pass


def on_unsubscribe_topic(mid, userdata):
    """
    取消订阅topic回调
    """
    print("on_unsubscribe_topic mid:%d" % mid)
    pass


def on_publish_topic(mid, userdata):
    """
    发布topic消息成功回调
    """
    print("on_publish_topic mid:%d,userdata:%d" % mid, userdata)


def generate_sim_stat():
    """
    生成模拟的传感器数据（温度、湿度、甲醛浓度）
    :return:
    """
    return json.dumps({
        'time': datetime.now().timestamp(),  # 时间
        'temperature': -10 + random.random() * 50,  # 温度
        'humidity': random.random() * 100,  # 湿度
        'ch2o': random.random() * 3  # 甲醛浓度
    })


lk.on_device_dynamic_register = on_device_dynamic_register
lk.on_connect = on_connect
lk.on_disconnect = on_disconnect
lk.on_topic_message = on_topic_message
lk.on_subscribe_topic = on_subscribe_topic
lk.on_unsubscribe_topic = on_unsubscribe_topic
lk.on_publish_topic = on_publish_topic
lk.config_mqtt(secure="")
lk.connect_async()

while True:
    try:
        rc, mid = lk.publish_topic(lk.to_full_topic('/'+PRODUCT_KEY+'/'+DEVICE_NAME+'/'+TOPIC), generate_sim_stat())
        print('Publish:rc={0},mid={1}'.format(rc, mid))
    except Exception as e:
        print(e)
    time.sleep(5)
